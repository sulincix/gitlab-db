#include <stddef.h>
struct GitlabRepo {
  char token[50];
  char id[50];
  char filename[50];
  char *content;
};
struct MemoryStruct {
  char *memory;
  size_t size;
};
//connection functions
//  reader
int create_file(struct GitlabRepo git);
int write_file(struct GitlabRepo git);
int delete_file(struct GitlabRepo git);
//  writer
int check_file(struct GitlabRepo git);
char* read_file(struct GitlabRepo git);
//file.c
char* readlines(const char *filename);
//database.c
void init(struct GitlabRepo git);
void save(struct GitlabRepo git);
//string functions
char** strsplit( const char* s, const char* delim );
int strcount(char* buf,char* c);
