#include <openssl/sha.h>
#include <openssl/hmac.h>
#include <openssl/evp.h>
#include <openssl/bio.h>
#include <openssl/buffer.h>
#include <base64.h>
#include <string.h>

char *base64(const unsigned char *input){
  BIO *bmem, *b64;
  BUF_MEM *bptr;
  size_t length=strlen(input);

  b64 = BIO_new(BIO_f_base64());
  bmem = BIO_new(BIO_s_mem());
  b64 = BIO_push(b64, bmem);
  BIO_write(b64, input, length);
  BIO_flush(b64);
  BIO_get_mem_ptr(b64, &bptr);

  char *buff = (char *)malloc(bptr->length);
  memcpy(buff, bptr->data, bptr->length-1);
  buff[bptr->length-1] = 0;

  BIO_free_all(b64);

  return buff;
}

size_t calcdecode64len(const char* b64input){
	size_t len = strlen(b64input),
		padding = 0;

	if (b64input[len-1] == '=' && b64input[len-2] == '=')
		padding = 2;
	else if (b64input[len-1] == '=')
		padding = 1;

	return (len*3)/4 - padding;
}

char *decode64(unsigned char *input){
  BIO *b64, *bmem;
  size_t length=calcdecode64len(input)+1;
  char *buffer = (char *)malloc(length);
  memset(buffer, 0, length);

  bmem = BIO_new_mem_buf(input, -1);
  b64 = BIO_new(BIO_f_base64());
  bmem = BIO_push(b64, bmem);
  
  BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);
  BIO_read(bmem, buffer, length);

  BIO_free_all(bmem);

  return buffer;
}
