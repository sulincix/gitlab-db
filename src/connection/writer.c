#include <gitlab-db.h>
#include <string.h>
#include <stdlib.h>
#include <curl/curl.h>
#include <stdio.h>
#include <base64.h>
#include <common.h>

char * p(char*name,char*val){
	int tot=strlen(name)+strlen(val)+1;
	char *ret=(char*)malloc(sizeof(char)*tot);
	strcpy(ret,name);
	strcat(ret,"=");
	strcat(ret,val);
	return ret;
}

int create_file(struct GitlabRepo git){
CURL *curl;
  struct MemoryStruct mem;
  mem.memory = malloc(1);
  mem.size = 0; 
  CURLcode res;
  curl = curl_easy_init();
  char url[1024]="https://gitlab.com/api/v4/projects/";
  strcat(url,git.id);
  strcat(url,"/repository/files/");
  strcat(url,git.filename);
  if(curl) {
    struct curl_slist *chunk = NULL;
    char thead[100]="PRIVATE-TOKEN: ";
    strcat(thead,git.token);
    char *data=malloc(1024*sizeof(char));
    strcpy(data,p("file_path",git.filename));
    strcat(data,"&");
    strcat(data,p("branch","master"));
    strcat(data,"&");
    strcat(data,p("commit_message","create new file"));
    strcat(data,"&");
    strcat(data,p("content","\"\""));
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, data);
    chunk = curl_slist_append(chunk, thead);
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, chunk);
    curl_easy_setopt(curl,CURLOPT_URL,url);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writefunc);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &mem);
    fprintf(stderr,">>> Creating %s to remote",git.filename);
    res = curl_easy_perform(curl);
    fprintf(stderr," (done)");
    if(res != CURLE_OK)
      fprintf(stderr, "curl_easy_perform() failed: %s\n",
              curl_easy_strerror(res));
    curl_easy_cleanup(curl); 
    curl_slist_free_all(chunk);
    if(strcmp(mem.memory,"{\"message\":\"A file with this name already exists\"}")==0){
      printf("    %s already exists\n",git.filename);
      return 0;
    }
    return 1;
  }
}

int write_file(struct GitlabRepo git){

  CURL *curl;
  struct MemoryStruct mem;
  mem.memory = malloc(1);
  mem.size = 0; 
  CURLcode res;
  curl = curl_easy_init();
  char url[1024]="https://gitlab.com/api/v4/projects/";
  strcat(url,git.id);
  strcat(url,"/repository/files/");
  strcat(url,git.filename);
  if(curl) {
    struct curl_slist *chunk = NULL;
    char thead[100]="PRIVATE-TOKEN: ";
    strcat(thead,git.token);
    char *data=malloc(1024*sizeof(char));
    strcpy(data,p("file_path",git.filename));
    strcat(data,"&");
    strcat(data,p("branch","master"));
    strcat(data,"&");
    strcat(data,p("encoding","base64"));
    strcat(data,"&");
    strcat(data,p("commit_message","update file"));
    strcat(data,"&");
    char *content=git.content;
    char*b64content=base64(content);
    strcat(data,p("content",b64content));
    curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, data);
    chunk = curl_slist_append(chunk, thead);
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, chunk);
    curl_easy_setopt(curl,CURLOPT_URL,url);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writefunc);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &mem);
    fprintf(stderr,">>> Sending %s (length:%d)",git.filename,strlen(b64content));
    res = curl_easy_perform(curl);
    fprintf(stderr," (done)\n");
    if(res != CURLE_OK)
      fprintf(stderr, "curl_easy_perform() failed: %s\n",
              curl_easy_strerror(res));
    curl_easy_cleanup(curl); 
    curl_slist_free_all(chunk);
    if(strcmp(mem.memory,"{\"message\":\"A file with this name doesn't exist\"}")==0){
      fprintf(stderr,"    %s not exists\n",git.filename);
      return 0;
    }
    return 1;
  }
}

int delete_file(struct GitlabRepo git){
CURL *curl;
  struct MemoryStruct mem;
  mem.memory = malloc(1);
  mem.size = 0; 
  CURLcode res;
  curl = curl_easy_init();
  char url[1024]="https://gitlab.com/api/v4/projects/";
  strcat(url,git.id);
  strcat(url,"/repository/files/");
  strcat(url,git.filename);
  if(curl) {
    struct curl_slist *chunk = NULL;
    char thead[100]="PRIVATE-TOKEN: ";
    strcat(thead,git.token);
    char *data=malloc(1024*sizeof(char));
    strcpy(data,p("file_path",git.filename));
    strcat(data,"&");
    strcat(data,p("branch","master"));
    strcat(data,"&");
    strcat(data,p("commit_message","remove file"));
    curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "DELETE");
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, data);
    chunk = curl_slist_append(chunk, thead);
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, chunk);
    curl_easy_setopt(curl,CURLOPT_URL,url);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writefunc);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &mem);

    res = curl_easy_perform(curl);
    fprintf(stderr,">>> Removing %s from remote\n",git.filename);
    if(res != CURLE_OK)
      fprintf(stderr, "curl_easy_perform() failed: %s\n",
              curl_easy_strerror(res));
    curl_easy_cleanup(curl); 
    curl_slist_free_all(chunk);
    if(strcmp(mem.memory,"{\"message\":\"A file with this name doesn't exist\"}")==0){
      fprintf(stderr,"    %s not exists\n",git.filename);
      return 0;
    }
    return 1;
  }
}
