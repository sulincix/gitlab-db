#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curl/curl.h>
#include <gitlab-db.h>
#include <common.h>
 
char* read_file(struct GitlabRepo git){
  CURL *curl;
  CURLcode res;
  curl = curl_easy_init();
  char url[1024]="https://gitlab.com/api/v4/projects/";
  strcat(url,git.id);
  strcat(url,"/repository/files/");
  strcat(url,git.filename);
  strcat(url,"/raw?ref=master");
  if(curl) {
    struct curl_slist *chunk = NULL;
    struct MemoryStruct mem;
    mem.memory = malloc(1);
    mem.size = 0; 
    char thead[100]="PRIVATE-TOKEN: ";
    strcat(thead,git.token);
    chunk = curl_slist_append(chunk, thead);
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, chunk);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writefunc);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &mem);
    curl_easy_setopt(curl,CURLOPT_URL,url);
    fprintf(stderr,"<<< Receiving %s from remote",git.filename);
    res = curl_easy_perform(curl);
    fprintf(stderr," (done)\n");
    if(res != CURLE_OK)
      fprintf(stderr, "curl_easy_perform() failed: %s\n",
              curl_easy_strerror(res));
    curl_easy_cleanup(curl); 
    curl_slist_free_all(chunk);
    if (strcmp("{\"message\":\"404 File Not Found\"}",mem.memory)==0){
	fprintf(stderr,"    File %s not found\n",git.filename);
	return "";
    }else{
	return mem.memory;
    }
  }
}
int check_file(struct GitlabRepo git){
CURL *curl;
  CURLcode res;
  curl = curl_easy_init();
  char url[1024]="https://gitlab.com/api/v4/projects/";
  strcat(url,git.id);
  strcat(url,"/repository/files/");
  strcat(url,git.filename);
  strcat(url,"/blame?ref=master");
  if(curl) {
    struct curl_slist *chunk = NULL;
    struct MemoryStruct mem;
    mem.memory = malloc(1);
    mem.size = 0; 
    char thead[100]="PRIVATE-TOKEN: ";
    strcat(thead,git.token);
    chunk = curl_slist_append(chunk, thead);
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, chunk);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writefunc);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &mem);
    curl_easy_setopt(curl,CURLOPT_URL,url);
   fprintf(stderr,"<<< Checking %s from remote",git.filename);
    res = curl_easy_perform(curl);
   fprintf(stderr," (done)\n",git.filename);
    if(res != CURLE_OK)
      fprintf(stderr, "curl_easy_perform() failed: %s\n",
              curl_easy_strerror(res));
    curl_easy_cleanup(curl); 
    curl_slist_free_all(chunk);
    if (strcmp("{\"message\":\"404 File Not Found\"}",mem.memory)){
    	return 0;
    }else{
    	return 1;
    }
  }
}
