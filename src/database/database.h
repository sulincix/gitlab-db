struct GitlabObject{
  char name[20];
  char value[1024];
};
//Get values
int getInt(char*val,int def);
char* getString(char*val,char* def);
//set values
void setInt(char*val,int def);
void setString(char*val,char* def);
//content modifications
char* getContent();
void setContent(char*ctx);
