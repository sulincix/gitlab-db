#include <database.h>
#include <gitlab-db.h>
#include <stdio.h>
#include <stdlib.h>
#include <common.h>
#include <string.h>
struct GitlabObject *obj;
int length=0;
void init(struct GitlabRepo git){
	char*content=read_file(git);
	char** array=strsplit(content,"\n");
	length=strcount(content,"\n");
	obj=malloc(sizeof(struct GitlabObject)*length);
	for(int i=0;i<length;i++){
		char** o=strsplit(array[i],"=");
		strcpy(obj[i].name,o[0]);
		strcpy(obj[i].value,o[1]);
	}
  
}
void setContent(char *ctx){
	char** array=strsplit(ctx,"\n");
	length=strcount(ctx,"\n");
	obj=malloc(sizeof(struct GitlabObject)*length);
	for(int i=0;i<length;i++){
		char** o=strsplit(array[i],"=");
		strcpy(obj[i].name,o[0]);
		strcpy(obj[i].value,o[1]);
	}
  
}

void save(struct GitlabRepo git){
	FILE *ctx=fopen(git.filename,"w");
	for(int i=0;i<length;i++){
		fprintf(ctx,"%s",obj[i].name);
		fprintf(ctx,"=");
		fprintf(ctx,"%s",obj[i].value);
		fprintf(ctx,"\n");
	}
	fclose(ctx);

}
char* getContent(){
  int j=0;
  for(int i=0;i<length;i++){
		j=j+strlen(obj[i].name);
    j=j+strlen(obj[i].value);
    j=j+2;
		
	}
  char *ctx=malloc(sizeof(char)*j);
  memset(ctx,0,0);
  for(int i=0;i<length;i++){
		strcat(ctx,obj[i].name);
		strcat(ctx,"=");
		strcat(ctx,obj[i].value);
		strcat(ctx,"\n");
	}
  return ctx;
}
char* getString(char *name,char*def){
	for(int i=0;i<length;i++){
		if(strcmp(obj[i].name,name)==0){
			return obj[i].value;
		}
	}
	return def;
}
int getInt(char *name,int def){
        char defs[1024];
        sprintf(defs, "%d", def);
	return atoi(getString(name,defs));
}
void setString(char* name,char*value){
	for(int i=0;i<length;i++){
		if(strcmp(obj[i].name,name)==0){
			strcpy(obj[i].value,value);
		}
	}	
}
void setInt(char*name,int value){
        char defs[1024];
        sprintf(defs, "%d", value);
        setString(name,defs);
}
