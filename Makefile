libsrc=src/connection/reader.c src/connection/writer.c 
libsrc+=src/misc/file.c src/misc/base64.c src/common/common.c src/database/database.c
src=src/main.c
ops= -lcurl -lssl -lcrypto -fPIC -O3
ops+=-Isrc/database -Isrc/misc -Isrc/common -Isrc/connection -Isrc
CC=gcc

DESTDIR=/
prefix=/usr
all: clean build-lib

build-lib:
	mkdir -p output/lib/pkgconfig 2>/dev/null || true
	$(CC) -o output/lib/libgitlabdb.so $(libsrc) $(ops) -shared $(CFLAGS)
	mkdir -p output/include/gitlab-db 2>/dev/null || ture
	install src/gitlab-db.h output/include/gitlab-db/gitlab-db.h
	install src/database/database.h output/include/gitlab-db/database.h
	install src/misc/base64.h output/include/gitlab-db/base64.h
	install src/gitlab-db.pc output/lib/pkgconfig/gitlab-db.pc
clean:
	rm -rf output
	make -C example clean
run:
	LD_LIBRARY_PATH=./output ./output/gitlab-db
	
install:
	cp -prfv ./output/* $(DESTDIR)/$(prefix)/
	
examples:
	make -C example
